#
# Copyright 2016, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Django command for processing access log files.
"""

import os
import sys

from django.core.management.base import BaseCommand

from stats.settings import get_setting
from stats.parser import parse_file, get_logs_rex
from stats.utils import LogOpen
from stats.counter import Counters

class Command(BaseCommand):
    """Command"""
    help = "Process yesterday's log files into the database"

    def add_arguments(self, parser):
        parser.add_argument('--file', dest='fname', default='access.log.1',
            help='The filename of both the nginx and wasgi logs.')
        parser.add_argument('--retry', action='store_true', dest='retry', default=False,
            help='Retry file even if it already exists')
        parser.add_argument('--lines', dest='count', type=int, default=None,
            help='Number of lines before quitting (implies debug).')
        parser.add_argument('--bad', action='store_true', dest='bad', default=False,
            help='Don\'t save anything, report bad lines instead.')
        parser.add_argument('--report', action='store_true', dest='report', default=False,
            help='Verbose reports on counters, aborts the commit.')

    def handle(self, fname, retry, count, bad, report, **options):
        self.last_lines = 0
        self.report = report
        if not fname.startswith('/'):
            fname = os.path.join(get_setting('ROOT'), 'nginx', fname)
        self.counters = Counters()
        if bad:
            logs = get_logs_rex('nginx')
            bad_count = 0
            with LogOpen(fname, logs['rex'], bad=True, reset=retry) as log:
                for line in log:
                    bad_count += 1
                    print(line)
                    if bad_count % 100 == 0:
                        pc = bad_count / log.count * 100
                        sys.stderr.write(f" * {bad_count} of {log.count} ({pc:.2f}%)\n")
                sys.stderr.write(f" * {bad_count} of {log.count}\n")
        else:
            results = parse_file('nginx', fname, retry, on_break=self.commit, count=count)
            self.counters.process_results(results)
            self.counters.commit()

    def commit(self, size, tell, line_count, dtm):
        """When the parser reaches a limit, it will break and signal us here"""
        this_lines = line_count - self.last_lines
        pct = "%.2f" % ((float(tell) / size) * 100)
        sys.stdout.write(f" {pct}% ({line_count}) {dtm} (lines: {this_lines}, db:")
        sys.stdout.flush()
        report, count = self.counters.commit(commit=not self.report)
        if self.report:
            report = ", ".join([f"{n}:{c}" for n,c in report.items()])
            print(f"{count}, {report})")
        else:
            print(f"{count})")
        self.last_lines = line_count
