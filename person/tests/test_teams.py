#
# Copyright 2015, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom 
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test team functions
"""
import json
from datetime import timedelta
from collections import defaultdict

from extratest.base import ExtraTestCase
from django.core import mail

from django.utils.timezone import now

from alerts.models import UserAlertSetting
from ..alert import RequestToJoinAlert, InviteToJoinAlert
from ..models import Team, Group, User
from inkscape.tests import reboot_alerts

URL_TO_LIST = {
    'join': 'members',
    'leave': 'old_members',
    'watch': 'watchers',
    'unwatch': 'old_watchers',
    'remove': 'old_members',
    'approve': 'members',
    'disapprove': 'old_requests',
    '': 'requests',
}
MSG = {
    'approved': "Approval of membership saved.",
    'not-allowed': 'add user to team. (not allowed)',
    'watching': "Now watching this team.",
    'not-watching': "No longer watching this team.",
    'no-watching': "You can't watch this team.",
    'not-removed': 'Cannot remove user from team. (not allowed)',
    'not-allowed': "Can't add user to team. (not allowed)",
    'already': "You are already a member.",
    'request': "Membership Request Received.",
    'request-already': "Already requested a membership to this team.",
    'request-removed': "User removed from membership requests.",
    'membership-added': 'Team membership sucessfully added.',
    'membership-accepted': 'Membership Invitation Accepted.',
    'sent-to-user': 'Invitation to join this team sent to user.',
    'already-sent': 'Invitation to join this team already sent to user.',
    'no-invite': "This user has not requested membership or replied to an invitation.",
    'removed': "User removed from team.",
    'disapproved': "Disapproval added."
}
ERR = [
    "User %s NOT found in list:%s when expected",
    "User %s found in list:%s, but expected it to be missing",
]

class TeamBase(object):
    fixtures = ('test-auth', 'test-teams')

    def setUp(self):
        super().setUp()
        reboot_alerts()

    def login(self, username, password=True):
        self.user = User.objects.get(username=username)
        if password is True:
            password = '123456'
            self.user.is_active = True
            self.user.set_password(password)
            self.user.save()
        self.client.login(username=username, password=password)

    def assertAction(self, team_id, url, msg=None, _not=False, approval=None, disapproval=None, **kw):
        try:
            team = Team.objects.get(slug=team_id)
            user = User.objects.get(username=kw['username']) \
                if 'username' in kw else self.user
        except User.DoesNotExist:
            self.assertFalse(True, "User '%s' doesn't exist" % kw['username'])

        list_name = kw.pop('list_name', URL_TO_LIST[url])
        #self._debug(team, url, user=user.pk, **kw)
        response = self.assertGet('team.'+url, team=team_id, status=302, **kw)
        #self._debug(team, url, user=user.pk, **kw)

        lst = getattr(team, list_name)
        # Make sure the action caused (or did not cause) a change in the
        # list of members for the related list. i.e. join changed members
        err = ERR[_not] % (user.pk, self._user_list(url, lst))
        self.assertEqual(lst.filter(user=user).count(), int(not _not), err)

        # When changing 'members' the permission group should change too.
        if list_name.endswith('members'):
            # When it's an old member, or the action failed
            if list_name.startswith('old') is _not:
                self.assertIn(user, team.group.user_set.all())
            else:
                self.assertNotIn(user, team.group.user_set.all())

        if msg is not None:
            # This is better than assertMessage because it clears the stack
            msgs = json.loads(self.assertGet('messages.json').content)['messages']
            self.assertIn(MSG.get(msg, msg), msgs)

        if approval is not None:
            self.assertEqual(team.approvals(user, approves=True).count(), approval)
        if disapproval is not None:
            self.assertEqual(team.approvals(user, approves=False).count(), disapproval)

    def assertNotAction(self, team, url, msg=None, **kw):
        return self.assertAction(team, url, msg=msg, _not=True, **kw)

    def assertAlert(self, cls, email):
        """Test if an alert (email) has been sent out to the team"""
        alerts = cls.get_alert_type().sent.all()
        emails = [alert.user.email for alert in alerts]
        self.assertIn(email, emails)

    def _debug(self, team, url, **kw):
        print(f" -= DEBUG ({url}:{kw}) =-")
        for name in set(URL_TO_LIST.values()):
            print(self._user_list(name, getattr(team, name)))
        print("")

    def _user_list(self, name, lst):
        return "%s: %s" % (name, ", ".join([str(pk)
            for pk in lst.values_list('user_id', flat=True)]))


class TeamTests(TeamBase, ExtraTestCase):
    credentials = dict(username='tester', password=True)

    def test_01_team_details(self):
        response = self.assertGet('team', team='c_team')
        self.assertContains(response, 'England')

    def test_02_secret_not_visible(self):
        """Secret teams are not visisble on the page"""
        response = self.assertGet('teams')
        self.assertNotContains(response, 'Atlantis')
        response = self.assertGet('team', team='s_team', status=404)

    def test_03_watch_secret_team(self):
        """Fail to watch a secret team"""
        self.assertNotAction('s_team', 'watch', msg='no-watching')

    def test_04_join_closed_team_user(self):
        """Fail to join a closed team"""
        self.assertNotAction('c_team', 'join', msg='not-allowed')

    def test_05_add_request_to_join(self):
        """Successfully request to join a team"""
        self.assertAction('t_team', 'join', 'request', list_name='requests')
        self.assertAction('t_team', 'join', 'request-already', list_name='requests')
        # XXX Is visible on page too
        self.assertAction('t_team', 'leave', 'request-removed', list_name='old_requests')

    def test_06_join_open_team(self):
        """Any user can join an open team"""
        self.assertAction('o_team', 'join', msg='membership-added')

    def test_07_watch_team(self):
        """A user can watch an open team"""
        self.assertAction('o_team', 'watch', msg='watching')

    def test_08_no_removal_by_user(self):
        """Random other user can not remove from team"""
        self.assertNotAction('o_team', 'remove', msg='not-removed', username='team_peer')

class TeamAdminTests(TeamBase, ExtraTestCase):
    credentials = dict(username='team_admin', password=True)

    def test_01_add_closed_team_admin(self):
        """Fail to add another user to a closed team"""
        self.assertNotAction('c_team', 'approve', msg='not-allowed', username='team_requester')

    def test_02_add_user_by_admin(self):
        """Successfully add requested users to team by admin"""
        self.assertAction('t_team', 'approve', msg='membership-added', username='team_requester')

    def test_03_remove_from_team_by_admin(self):
        """User removed by admin from team"""
        self.assertAction('t_team', 'remove', msg='removed', username='team_peer')
        self.assertAction('t_team', 'remove', msg='request-removed', username='team_requester', list_name='old_requests')


class TeamPeerTests(TeamBase, ExtraTestCase):
    credentials = dict(username='team_peer', password=True)

    def test_01_add_user_by_peer(self):
        """Fail to approve a user by a peer"""
        self.assertNotAction('t_team', 'approve', msg='not-allowed', username='team_requester')

    def test_02_add_user_by_peer(self):
        """Successfully add requested users to team by peer"""
        self.assertAction('p_team', 'approve', msg='membership-added', username='team_requester', approval=1)
        self.assertNotAction('p_team', 'approve', msg='sent-to-user', username='tester', approval=1)
        self.assertAlert(InviteToJoinAlert, 'test@example.com')
        self.assertNotAction('p_team', 'approve', msg='no-invite', username='tester', approval=1)
        self.assertNotAction('p_team', 'approve', msg='sent-to-user', username='team_watcher', approval=1)

    def assertEmails(self, team_slug, count=1, *msgs):
        """Make sure we have a report for the admins"""
        user = User.objects.get(username=f'list.{team_slug}')

        settings = defaultdict(list)
        for user_id, alert_id in user.alert_settings\
                .filter(batch='W', email=True)\
                .values_list('user_id', 'alert_id'):
            settings[user_id].append(alert_id)

        for user_id, alert_ids in settings.items():
            qset = user.alerts.filter(viewed__isnull=True, deleted__isnull=True)
            qset.send_batch_email(batch_mode='W', user_id=user_id, alert_ids=alert_ids)

        self.assertEqual(len(mail.outbox), count)
        for x, msg in enumerate(msgs):
            if msg is not None:
                self.assertEqual(mail.outbox[x].subject, msg)

    def test_02_invite_peer(self):
        """Peers can add other users"""
        self.login('team_peer_one')
        self.assertNotAction('p3_team', 'approve', msg='sent-to-user', username='tester', approval=1)
        self.assertAlert(InviteToJoinAlert, 'test@example.com')
        self.login('team_peer_two')
        self.assertNotAction('p3_team', 'approve', msg='already-sent', username='tester', approval=2)
        self.login('team_peer_three')
        self.assertNotAction('p3_team', 'approve', msg='already-sent', username='tester', approval=3)
        self.login('team_peer_four')
        self.assertNotAction('p3_team', 'approve', msg='already-sent', username='tester', approval=4)
        self.login('tester')
        self.assertAction('p3_team', 'join', msg='membership-accepted', approval=4)
        self.assertEmails('p3_team', 2, "Team invitation: Portugal", "Membership report: Portugal")

    def test_02_request_peer(self):
        """User can be peer approved"""
        self.login('tester')
        self.assertNotAction('p3_team', 'join', msg='request', approval=0)
        self.assertAlert(RequestToJoinAlert, 'admin@inkscape.org')
        self.login('team_peer_one')
        self.assertNotAction('p3_team', 'approve', msg='approved', username='tester', approval=1)
        self.login('team_peer_two')
        self.assertNotAction('p3_team', 'approve', msg='approved', username='tester', approval=2)
        self.login('team_peer_three')
        self.assertAction('p3_team', 'approve', msg='membership-added', username='tester', approval=3)
        self.assertEmails('p3_team', 2, "Team request: Portugal", "Membership report: Portugal")

    def test_02_disaproval_peer(self):
        """Use can not be added with disapproval"""
        self.login('tester')
        self.assertNotAction('p3_team', 'join', msg='request', approval=0)
        self.login('team_peer_one')
        self.assertNotAction('p3_team', 'disapprove', msg='disapproved', username='tester', approval=0, disapproval=1)
        self.login('team_peer_two')
        self.assertNotAction('p3_team', 'disapprove', msg='disapproved', username='tester', approval=0, disapproval=2)
        self.login('team_peer_three')
        self.assertNotAction('p3_team', 'disapprove', msg='disapproved', username='tester', approval=0, disapproval=3)
        self.login('team_peer_four')
        self.assertNotAction('p3_team', 'disapprove', msg='disapproved', username='tester', approval=0, disapproval=4)
        self.login('tester')
        self.assertNotAction('p3_team', 'join', msg='request-already', approval=0)

    def test_03_join_open_team(self):
        """Fail to rejoin open team when already a member"""
        self.assertAction('o_team', 'join', msg='already')

    def test_04_no_removal_by_peer(self):
        """Peer can not remove user from team"""
        self.assertAction('o_team', 'approve', username="team_requester")
        self.assertNotAction('o_team', 'remove', msg="not-removed", username="team_requester")

    def test_05_remove_from_team_by_user(self):
        """User removes themselves from team"""
        self.assertAction('o_team', 'leave', msg='removed')

    def test_06_join_closed_team_peer(self):
        """Fail to join a closed team as a peer"""
        self.assertNotAction('c_team', 'approve', msg='not-allowed', username='team_requester')

    def test_07_leave_and_cant_rejoin(self):
        self.assertAction('t_team', 'leave')
        self.assertNotAction('t_team', 'join')

    def test_08_leave_and_rejoin(self):
        self.assertAction('o_team', 'leave')
        self.assertAction('o_team', 'join')

    def test_09_expires_future(self):
        """An expiration in the future"""
        membership = self.user.memberships.get(team__slug='o_team')
        self.assertTrue(membership.expired is None)
        self.assertEqual(membership.team.members.count(), 1)
        membership.expired = now() + timedelta(days=1)
        membership.save()
        self.assertFalse(membership.expired is None)
        self.assertFalse(membership.is_expired)
        self.assertEqual(membership.team.members.count(), 1)
        self.user.save()
        self.assertIn(self.user, membership.team.group.user_set.all())
        membership.expired = now() - timedelta(days=1)
        membership.save()
        self.assertTrue(membership.is_expired)
        self.assertEqual(membership.team.members.count(), 0)
        self.assertNotIn(self.user, membership.team.group.user_set.all())


class TeamWatcherTests(TeamBase, ExtraTestCase):
    credentials = dict(username='team_watcher', password=True)

    def test_01_join_open_team(self):
        """Any user can join an open team"""
        self.assertAction('o_team', 'join', msg='membership-added')

    def test_02_watch_then_join_team(self):
        """Watch and then join a team"""
        self.assertAction('o_team', 'join', msg='membership-added')

    def test_03_watch_unwatch(self):
        """Watch and then unwatch a team"""
        self.assertAction('o_team', 'unwatch', msg='not-watching')


class TeamRequesterTests(TeamBase, ExtraTestCase):
    credentials = dict(username='team_requester', password=True)

    def test_01_remove_from_team_by_requester(self):
        """User removes themselves from requests list"""
        self.assertAction('o_team', 'leave', msg='request-removed', list_name="old_requests")

